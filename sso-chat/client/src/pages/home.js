import React from 'react';
import {Link} from 'react-router-dom';

const Home = () => {

  return  <>
    <h1>Welcome</h1>
    <Link to="/sso/login">To login</Link>
  </>
}

export default Home;